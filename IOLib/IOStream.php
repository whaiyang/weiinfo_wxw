<?php
	require_once("weiIO.php");
	
	class IOStream
	{
		public static $WEI_IO = "weiIO";
	
		private $baseIO = null;
		
		public function __construct($type=null,$data=null)
		{
			if(isset($type)&&is_string($type))
			{
				$this->baseIO = new $type($data);
			}
		}
		
		public function setType($type,$data=null)
		{
			$this->baseIO = new $type($data);
		}
		
		public function open($take=false)
		{
			return $this->baseIO->open($take);
		}
		
		public function close()
		{
			return $this->baseIO->close();
		}
		
		public function getMSG()
		{
			return $this->baseIO->getMSG();
		}
		
		public function putMSG($msg,$take=false)
		{
			$this->baseIO->putMSG($msg,$take);
		}
	}