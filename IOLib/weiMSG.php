<?php
	/**
		description:存储微信推送的消息,以及对消息的相关操作
		author:王海阳
		time:2013/4/18
	*/
	require_once("baseIO.interface.php");
	require_once("MSG.php");
	
	abstract class weiMSG extends MSG
	{
		protected $toUserName;
		protected $fromUserName;
		protected $msgID;
		
		public function __construct($time,$type,$toUserName,$fromUserName,$msgID)
		{
			parent::__construct($time,$type);
			$this->toUserName = $toUserName;
			$this->fromUserName = $fromUserName;
			$this->msgID = $msgID;
		}
		
		public function getTUserN()
		{
			return $this->toUserName;
		}
		
		public function getFUserN()
		{
			return $this->fromUserName;
		}
		
		public function getMsgID()
		{
			return $this->msgID;
		}
		
		public function changeTarget()
		{
			$tmp = $this->toUserName;
			$this->toUserName = $this->fromUserName;
			$this->fromUserName = $tmp;
		}
		
		public function response($data){}
	}
	
	//由已有消息创建空消息
	class weiMSGBuilder
	{
		public static $MSG_TEXT = "text";
		public static $MSG_NEWS = "news";
		public static $MSG_MUSIC = "music";
		public static $MSG_VOICE = "voice";
		public static function build($msg,$type,$specData=null)
		{
			$newMsg;
			switch($type)
			{
				case "text":
					$data = array("time"=>$msg->getMSGCreateTime(),"type"=>"text",'toUserName'=>$msg->getTUserN(),'fromUserName'=>$msg->getFUserN(),'msgID'=>$msg->getMsgID(),'content'=>"");
					$newMsg = new weiTextMSG($data);
					break;
				case "news":
					$data = array("time"=>$msg->getMSGCreateTime(),"type"=>"news",'toUserName'=>$msg->getTUserN(),'fromUserName'=>$msg->getFUserN(),'msgID'=>$msg->getMsgID());
					$newMsg = new weiNewsMSG($data);
					break;
				case "music":
					$data = array("time"=>$msg->getMSGCreateTime(),"type"=>"music",'toUserName'=>$msg->getTUserN(),'fromUserName'=>$msg->getFUserN(),'msgID'=>$msg->getMsgID());
					$newMsg = new weiMusicMSG($data);
					break;
				case "voice":
					$data = array("time"=>$msg->getMSGCreateTime(),"type"=>"voice",'toUserName'=>$msg->getTUserN(),'fromUserName'=>$msg->getFUserN(),'msgID'=>$msg->getMsgID());
					$newMsg = new weiVoiceMSG($data);
					break;
			}
			return $newMsg;
		}
	}
	
	//由xml解析的数据创建消息
	class weiMSGFactory
	{
		public static function create($msgType,$xml)
		{
			//$class = "wei".ucfirst($msgType)."MSG";
			//return new $class($class::parseXML($xml));
			switch ($msgType) {
				case 'text':
					return new weiTextMSG(weiTextMSG::parseXML($xml));
					break;
				case 'location':
					return new weiLocationMSG(weiLocationMSG::parseXML($xml));
					break;
				case 'link':
					return new weiLinkMSG(weiLinkMSG::parseXML($xml));
					break;
				case 'image':
					return new weiImageMSG(weiImageMSG::parseXML($xml));
					break;
				case 'music':
					return new weiMusicMSG(weiMusicMSG::parseXML($xml));
					break;
				case 'event':
					return new weiEventMSG(weiEventMSG::parseXML($xml));
					break;
			}
		}
	}
	
	class SendWeiMSG
	{
		/**
			由weiIO->putMSG 调用
		*/
		public static function createXML($msg)
		{
			$xml = "";
			switch($msg->getMSGType())
			{
				case "text":
					$textTpl = "<xml>
								<ToUserName><![CDATA[%s]]></ToUserName>
								<FromUserName><![CDATA[%s]]></FromUserName>
								<CreateTime>%s</CreateTime>
								<MsgType><![CDATA[%s]]></MsgType>
								<Content><![CDATA[%s]]></Content>
								<FuncFlag>1</FuncFlag>
								</xml>";
					$xml = sprintf($textTpl,$msg->getTUserN(),$msg->getFUserN(),time(),$msg->getMSGType(),$msg->getContent());
				break;
				case "voice":
					$textTpl = "<xml>
								<ToUserName><![CDATA[%s]]></ToUserName>
								<FromUserName><![CDATA[%s]]></FromUserName>
								<CreateTime>%s</CreateTime>
								<MsgType><![CDATA[%s]]></MsgType>
								<Voice>
									<MediaId><![CDATA[%s]]></MediaId>
								</Voice>
								<FuncFlag>1</FuncFlag>
								</xml>";
					$xml = sprintf($textTpl,$msg->getTUserN(),$msg->getFUserN(),time(),$msg->getMSGType(),$msg->getMediaId());
				break;
				case "news":
					$textTpl = "<xml>
								<ToUserName><![CDATA[%s]]></ToUserName>
								<FromUserName><![CDATA[%s]]></FromUserName>
								<CreateTime>%s</CreateTime>
								<MsgType><![CDATA[%s]]></MsgType>
								<ArticleCount>%s</ArticleCount>
								<Articles>";
					$article = "<item>
								<Title><![CDATA[%s]]></Title> 
								<Description><![CDATA[%s]]></Description>
								<PicUrl><![CDATA[%s]]></PicUrl>
								<Url><![CDATA[%s]]></Url>
								</item>";
					$articleCount = $msg->getArticleCount();
					$articles = "";
					$arr = null;
					$textTpl = sprintf($textTpl,$msg->getTUserN(),$msg->getFUserN(),time(),$msg->getMSGType(),$articleCount);
					for($i=0;$i<$articleCount;$i++)
					{
						$arr = $msg->getItem($i);
						$articles.=sprintf($article,$arr['title'],$arr['description'],$arr['picUrl'],$arr['url']);
					}
					$textTpl.=$articles;
					$textTpl.=" </Articles>
								<FuncFlag>1</FuncFlag>
								</xml>";
					$xml = $textTpl;
					break;
				case "music":
					$textTpl = "<xml>
								<ToUserName><![CDATA[%s]]></ToUserName>
								<FromUserName><![CDATA[%s]]></FromUserName>
								<CreateTime>%s</CreateTime>
								<MsgType><![CDATA[%s]]></MsgType>
								<Music>
									<Title><![CDATA[%s]]></Title>
									<Description><![CDATA[%s]]></Description>
									<MusicUrl><![CDATA[%s]]></MusicUrl>
									<HQMusicUrl><![CDATA[%s]]></HQMusicUrl>
								</Music>
								<FuncFlag>1</FuncFlag>
								</xml>";
					$xml = sprintf($textTpl,$msg->getTUserN(),$msg->getFUserN(),time(),$msg->getMSGType(),$msg->getTitle(),$msg->getDescription(),$msg->getMusicUrl(),$msg->getHQMusicUrl());
					break;
			}
			return $xml;
		}
	}
	
	//文本消息
	class weiTextMSG extends weiMSG
	{
		private $content;
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
			$this->content = $data['content'];
		}
		
		public static function parseXML($xml)
		{
			$TUserN = $xml->ToUserName;
			$FUserN = $xml->FromUserName;
			$createTime = $xml->CreateTime;
			$msgType = $xml->MsgType;
			$content = $xml->Content;
			$msgID = $xml->MsgId;
			return array('time'=>$createTime,'type'=>$msgType,'toUserName'=>$TUserN,
			'fromUserName'=>$FUserN,'msgID'=>$msgID,'content'=>$content);
		}
		
		public function setContent($str)
		{
			if(is_string($str))
			{
				$this->content = $str;
				return true;
			}
			return false;
		}
		
		public function getContent()
		{
			return $this->content;
		}
		
		public function response($content)
		{
			$this->content = $content;
			$this->changeTarget();
		}
	}
	
	//地理位置消息
	class weiLocationMSG extends weiMSG
	{
		private $Location_X;
		private $Location_Y;
		private $Scale;
		private $Label;
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
			$this->Location_X = $data['location_x'];
			$this->Location_Y = $data['location_y'];
			$this->Scale = $data['scale'];
			$this->Label = $data['label'];
		}
		
		public static function parseXML($xml)
		{
			$TUserN = $xml->ToUserName;
			$FUserN = $xml->FromUserName;
			$createTime = $xml->CreateTime;
			$msgType = $xml->MsgType;
			$location_x = $xml->Location_X;
			$location_y = $xml->Location_Y;
			$scale = $xml->Scale;
			$label = $xml->Label;
			$msgID = $xml->MsgId;
			return array('time'=>$createTime,'type'=>$msgType,'toUserName'=>$TUserN,
			'fromUserName'=>$FUserN,'msgID'=>$msgID,'location_x'=>$location_x,'location_y'=>$location_y,'scale'=>$scale,'label'=>$label);
		}
		
		public function getLabel()
		{
			return $this->Label;
		}
		
		public function getLocation_X()
		{
			return $this->Location_X;
		}
		
		public function getLocation_Y()
		{
			return $this->Location_Y;
		}
		
		public function getScale()
		{
			return $this->Scale;
		}
	}
	//连接消息
	class weiLinkMSG extends weiMSG
	{
		private $Title;
		private $Description;
		private $Url;
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
			$this->Title = $data['title'];
			$this->Description = $data['description'];
			$this->Url = $data['url'];
		}
		
		public static function parseXML($xml)
		{
			$TUserN = $xml->ToUserName;
			$FUserN = $xml->FromUserName;
			$createTime = $xml->CreateTime;
			$msgType = $xml->MsgType;
			$title = $xml->Title;
			$description = $xml->Description;
			$url = $xml->Url;
			$msgID = $xml->MsgId;
			return array('time'=>$createTime,'type'=>$msgType,'toUserName'=>$TUserN,
			'fromUserName'=>$FUserN,'msgID'=>$msgID,'title'=>$title,'description'=>$description,'url'=>$url);
		}
		
		
		public function getTitle()
		{
			return $this->Title;
		}
		
		public function getDescription()
		{
			return $this->Description;
		}
		
		public function getUrl()
		{
			return $this->Url;
		}
	}
	
	//图片消息
	class weiImageMSG extends weiMSG
	{
		private $PicUrl;
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
			$this->PicUrl = $data['picUrl'];
		}
		
		public static function parseXML($xml)
		{
			$TUserN = $xml->ToUserName;
			$FUserN = $xml->FromUserName;
			$createTime = $xml->CreateTime;
			$msgType = $xml->MsgType;
			$picUrl = $xml->PicUrl;
			$msgID = $xml->MsgId;
			return array('time'=>$createTime,'type'=>$msgType,'toUserName'=>$TUserN,
			'fromUserName'=>$FUserN,'msgID'=>$msgID,'picUrl'=>$picUrl);
		}
		
		public function getPicUrl()
		{
			return $this->PicUrl;
		}
	}
	
	//图文消息，用于回复
	class weiNewsMSG extends weiMSG
	{
		private $ArticleCount = 0;
		private $Articles = array();
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
		}
		
		public function getArticleCount()
		{
			return $this->ArticleCount;
		}
		
		/**
			添加一个图文消息
			$data = array('title'=>Title,'description'=>Description,'picUrl'=>PicUrl,'url'=>Url)
			Title	 		图文消息标题
			Description		图文消息描述
			PicUrl	 		图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80。
			Url	 			点击图文消息跳转链接
		*/
		public function addItem($data)
		{
			$this->Articles[] = array('title'=>$data['title'],'description'=>$data['description'],'picUrl'=>$data['picUrl'],'url'=>$data['url']);
			$this->ArticleCount++;
		}
		
		public function removeItem($index)
		{
			return array_splice($this->Articles,$index,1);
		}
		
		public function getItem($index)
		{
			return $this->Articles[$index];
		}
	}
	
	//音乐消息
	class weiMusicMSG extends weiMSG
	{
		private $Title;
		private $Description;
		private $MusicUrl;
		private $HQMusicUrl;//高质量音乐链接，WIFI环境优先使用该链接播放音乐
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
		}
		
		public function getTitle()
		{
			return $this->Title;
		}
		
		public function getDescription()
		{
			return $this->Description;
		}
		
		public function getMusicUrl()
		{
			return $this->MusicUrl;
		}
		
		public function getHQMusicUrl()
		{
			return $this->HQMusicUrl;
		}
		
		/**
			添加一个音乐
			$data = array('title'=>Title,'description'=>Description,'musicUrl'=>musicUrl,'HQMusicUrl'=>HQMusicUrl)
			MusicUrl	 音乐链接
			HQMusicUrl	 高质量音乐链接，WIFI环境优先使用该链接播放音乐
		*/
		public function addMusic($music)
		{
			$this->Title = $music['title'];
			$this->Description = $music['description'];
			$this->MusicUrl = $music['musicUrl'];
			$this->HQMusicUrl = $music['HQMusicUrl'];
			$this->changeTarget();
		}
	}
	
	//事件消息
	class weiEventMSG extends weiMSG
	{
		private $Event;//事件类型，subscribe(订阅)、unsubscribe(取消订阅)、CLICK(自定义菜单点击事件)
		private $EventKey;//事件KEY值，与自定义菜单接口中KEY值对应
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
			$this->Event = $data['Event'];
			$this->EventKey = $data['EventKey'];
		}
		
		public static function parseXML($xml)
		{
			$TUserN = $xml->ToUserName;
			$FUserN = $xml->FromUserName;
			$createTime = $xml->CreateTime;
			$msgType = $xml->MsgType;
			$event = $xml->Event;
			$eventKey = $xml->EventKey;
			$msgID = $xml->MsgId;
			return array('time'=>$createTime,'type'=>$msgType,'toUserName'=>$TUserN,
			'fromUserName'=>$FUserN,'msgID'=>$msgID,'Event'=>$event,'EventKey'=>$eventKey);
		}
		
		public function getEvent()
		{
			return $this->Event;
		}
		
		public function getEventKey()
		{
			return $this->EventKey;
		}
	}

	//语音消息
	class weiVoiceMSG extends weiMSG
	{
		private $MediaId;
		
		public function __construct($data)
		{
			parent::__construct($data['time'],$data['type'],$data['toUserName'],$data['fromUserName'],$data['msgID']);
		}
		
		public function setMediaId($MediaId)
		{
			$this->MediaId = $MediaId;
		}
		
		public function getMediaId()
		{
			return $this->MediaId;
		}
		
		public function response($content)
		{
			$this->changeTarget();
		}
	}