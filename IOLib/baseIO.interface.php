<?php
	interface IBaseIO
	{
		public function open();
		public function close();
		public function getMSG();
		public function putMSG($msg);
	}
	
	interface IMSG
	{
		public function getMSGCreateTime();
		public function getMSGType();
	}