<?php
	class Sql
	{
		//WeiUser
		public static function weiIdAvailable($wid,$db)
		{
			$result = $db->select(array("*"),array('weiid'=>$wid),WEIUSER);
			return empty($result);
		}
		
		public static function WeiUser_getNextId($tablename,$db)
		{
			$result =  $db->db->query("show table status where name = '".$tablename."'");
			$arr = $result->fetch_assoc();
			$nid = $arr['Auto_increment']+1;
			return $nid;
		}
		
		public static function WeiUser_getNextName($db)
		{
			$name = "我没名字".self::WeiUser_getNextId(WEIUSER,$db);
			return $name;
		}
		
		//若此微信号已注册，返回weiid_used
		public static function WeiUser_createUser($weiId,$db)
		{
			if(!self::weiIdAvailable($weiId,$db))
			{
				$json = '{"status":false,"error":"weiid_used"}';
				return json_decode($json);
			}
			$name = self::WeiUser_getNextName($db);
			if($db->insert(array('weiid'=>$weiId,'tmpname'=>$name,"created"=>DateHelper::getSqlTimeDate(),"regi"=>0),WEIUSER))
			{	
				$json = '{"status":true,"error":"","name":"'.$name.'"}';
				return json_decode($json);
			}
			else
			{
				$json = '{"status":false,"error":"db_error"}';
				return json_decode($json);
			}
		}
		//User
		public static function nameAvailable($name,$db)
		{
			$result = $db->select(array("*"),array('name'=>$name),USER);
			return empty($result);
		}
		
		public static function userAvailable($wid,$db)
		{
			$result = $db->select(array("*"),array('weiid'=>$wid),USER);
			return empty($result);
		}
		
		public static function userValid($user,$db)
		{
			$result = $db->select(array("*"),array('email'=>$user['email'],'password'=>md5($user['password'])),USER);
			return isset($result);
		}
		
		public static function changePassword($uid,$newPass,$db)
		{
			return $db->update(array("password"=>md5($newPass)),array('id'=>$uid),USER);
		}
		
		/*
			$user=>
				weiid: not null
				name:not null
				email:not null
				desc
			error type:
				already_registered
				weiid_null
				name_null
				password_null
				password_too_short
				name_too_short
				name_too_long
				name_used
				email_null
				email_format
				email_used
		*/
		public static function createUser($user,$db)
		{
			$len = intval(strlen($user['name']));
			$len2 = intval(strlen($user['password']));
			if(empty($user['name']))
				return "name_null";
			else if(empty($user['weiid']))
				return "weiid_null";
			else if(empty($user['password']))
				return "password_null";
			else if(empty($user['email']))
				return "email_null";
			else if(!preg_match("/^([0-9A-Za-z\-_\.]+)@([0-9a-z]+\.[a-z]{2,3}(\.[a-z]{2})?)$/i",$user['email']))
				return "email_format";
			else if($len<6)
				return "name_too_short";
			else if($len>30)
				return "name_too_long";
			else if($len2<6)
				return "password_too_short";
			else
			{
				if(self::userAvailable($user['weiid'],$db))
				{
					if(self::nameAvailable($user['name'],$db))
					{
						//$db->db->query("insert into user(`name`,`email`,`created`,`desc`) values('".$user['name']."','".$user['email']."','".DateHelper::getSqlTimeDate()."','".self::ifempty($user['desc'])."')")
						if($db->insert(array('weiid'=>$user['weiid'],'name'=>$user['name'],"password"=>md5($user['password']),"email"=>$user['email'],"created"=>DateHelper::getSqlTimeDate(),'desc'=>self::ifempty($user['desc'])),USER)&&$db->update(array('regi'=>1),array('weiid'=>$user['weiid']),WEIUSER))
						{
							return true;
						}
						else
							return false;
					}
					else
						return "name_used";
				}
				else
					return "already_registered";
			}
		}
	
		//Book
		//判断此用户是否已评价此书
		public static function bookAppraised($uid,$isbn,$db)
		{
			$result = $db->select(array("*"),array('auth_id'=>$uid,'isbn10'=>$isbn),BOOK_COMMENT);
			return isset($result);
		}
		
		/*
			$arr=>
				auth_id:not null
				isbn10:not null
				comment
				score
		*/
		public static function putBookComment($arr,$db)
		{
			
			$book_id = self::findBookByISBN10($arr['isbn'],$db);
			$book_id = $book_id['id'];
			if(self::bookAppraised($arr['uid'],$arr['isbn'],$db)&&$book_id)
				return false;
			else
			{
				$time = DateHelper::getSqlTimeDate();
				if($db->insert(array('auth_id'=>$arr['uid'],
					'isbn10'=>$arr['isbn'],
					'book_id'=>$book_id,
					'comment'=>self::ifempty($arr['comment']),
					'scores'=>self::ifempty($arr['score']),
					'published'=>$time,
					'updated'=>$time
				),BOOK_COMMENT))
					return true;
				else
					return false;
			}
		}
		
		/*
			$arr=>
				$auth_id:not null
				$isbn10:not null
				$comment
				$score
		*/
		public static function updateBookComment($arr,$db)
		{
			if(self::bookAppraised($arr['uid'],$arr['isbn'],$db))
			{
				$time = DateHelper::getSqlTimeDate();
				if($db->update(array('comment'=>self::ifempty($arr['comment']),'scores'=>self::ifempty($arr['score']),'updated'=>$time),array('auth_id'=>$arr['uid'],"isbn10"=>$arr['isbn']),BOOK_COMMENT))
					return true;
				else
					return false;
			}
			else
				return false;
		}
		
		public static function findBookByISBN10($isbn,$db)
		{
			$result =  $db->select(array("*"),array('isbn10'=>$isbn),BOOK);
			return $result[0];
		}
		
		/*
		$arr=>
			title: not null
			author: not null array()
			isbn10: not null
			isbn13
			images: image1|image2|image3
			translator: array()
			publisher
			pubdate
			tags:	array()
			author_intro
			summary
			avg_score
		*/
		public static function createBook($arr,$db)
		{
			if(empty($arr['title'])||empty($arr['author'])||empty($arr['isbn10'])||count($arr['author'])<=0)
				return false;
			else
			{
				if($db->insert(array('title'=>$arr['title'],
					'isbn10'=>self::ifempty($arr['isbn10']),
					'isbn13'=>self::ifempty($arr['isbn13']),
					'images'=>self::ifempty($arr['images']),
					'publisher'=>self::ifempty($arr['publisher']),
					'pubdate'=>self::ifempty($arr['pubdate']),
					'author_intro'=>self::ifempty($arr['author_intro']),
					'summary'=>self::ifempty($arr['summary']),
					'avg_score'=>self::ifempty($arr['avg_score'])
				),BOOK))
				{
					$book_id = $db->insert_id();
					foreach($arr['author'] as $author)
					{
						$db->insert(array("book_id"=>$book_id,"author"=>$author),BOOK_AUTHOR);
					}
					
					if(isset($arr['translator'])&&count($arr['translator'])>0)
					{
						
						foreach($arr['translator'] as $translator)
						{
							$db->insert(array("book_id"=>$book_id,"translator"=>$translator),BOOK_TRANSLATOR);
						}
					}
					
					if(isset($arr['tags'])&&count($arr['tags'])>0)
					{
						
						foreach($arr['tags'] as $tag)
						{
							$db->insert(array("book_id"=>$book_id,"tag"=>$tag),BOOK_TAG);
						}
					}
				}
				else
					return false;
			}
		}
		
		//Movie
		//判断此用户是否已评价此书
		public static function movieAppraised($uid,$mid,$type,$db)
		{
			$result = $db->select(array("*"),array('auth_id'=>$uid,'movie_id'=>$mid),$type);
			return isset($result);
		}
		
		public static function movieLongAppraised($uid,$mid,$db)
		{
			$result = $db->select(array("*"),array('auth_id'=>$uid,'movie_id'=>$mid),MOVIE_LONG_COMMENT);
			return isset($result);
		}
		
		public static function findMoviesByName($name,$db)//返回数组
		{
			return $db->select(array("*"),array('title'=>$name),MOVIE);
		}
		
		/*
			title: not null
			summary
			images: image1|image2|image3
			languages: languages1|languages2|languages3
			pubdate
			avg_score
			tags:array()
		*/
		public static function  createMovie($arr,$db)
		{
			if(empty($arr['title']))
				return false;
			else
			{
				if($db->insert(array('title'=>$arr['title'],
					'summary'=>Sql::ifempty($arr['summary']),
					'images'=>Sql::ifempty($arr['images']),
					'languages'=>Sql::ifempty($arr['languages']),
					'pubdate'=>Sql::ifempty($arr['pubdate']),
					'avg_score'=>Sql::ifempty($arr['avg_score'])),MOVIE))
				{
					$movie_id = $db->insert_id();
					if(isset($arr['tags'])&&count($arr['tags'])>0)
					{
						foreach($arr['tags'] as $tag)
						{
							$db->insert(array("movie_id"=>$movie_id,"tag"=>$tag),MOVIE_TAG);
						}
					}
				}
				else
					return false;
			}
		}
		
		/*
			$arr=>
				auth_id:not null
				mid:not null
				comment
				score
				type:MOVIE_SHORT_COMMENT or MOVIE_LONG_COMMENT
		*/
		public static function putMovieComment($arr,$type,$db)
		{
			if(empty($arr["mid"])||empty($arr["uid"])||self::movieAppraised($arr['uid'],$arr['mid'],$type,$db))
				return false;
			else
			{
				$time = DateHelper::getSqlTimeDate();
				if($db->insert(array('auth_id'=>$arr['uid'],
					'movie_id'=>$arr['mid'],
					'comment'=>self::ifempty($arr['comment']),
					'scores'=>self::ifempty($arr['score']),
					'published'=>$time,
					'updated'=>$time,
				),$type))
					return true;
				else
					return false;
			}
		}

		public static function updateMovieComment($arr,$type,$db)
		{
			if(empty($arr["mid"])||empty($arr["uid"])||!self::movieAppraised($arr['uid'],$arr['mid'],$type,$db))
				return false;
			else
			{
				$time = DateHelper::getSqlTimeDate();
				if($db->update(array(
					'comment'=>self::ifempty($arr['comment']),
					'scores'=>self::ifempty($arr['score']),
					'updated'=>$time,
				),array('auth_id'=>$arr['uid'],
					'movie_id'=>$arr['mid']),$type))
					return true;
				else
					return false;
			}
		}	
			
		public static function ifempty($v,$r="")
		{
			return empty($v)?$r:$v;
		}
	}