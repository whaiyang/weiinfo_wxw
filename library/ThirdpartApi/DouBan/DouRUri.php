<?php
	//豆瓣OAuth回调处理
	
	@$error = $_GET['error'];
	@$code = $_GET['code'];
	//出错
	if(!empty($error))
	{
		echo "error";
		exit();
	}
	//请求access_token
	if(isset($code))
	{
		SET_SESSION("code",$code);
		$dou_uri = "https://www.douban.com/service/auth2/token";
		$client_id = Dou_APIKey;
		$client_secret = Dou_secret;
		$grant_type = "authorization_code";
		$request_data = 'client_id='.$client_id.'&client_secret='.$client_secret.
					   '&redirect_uri='.Dou_redirect_uri.'&grant_type='.$grant_type.'&code='.$code;
		$result = do_post_request($dou_uri,$request_data);
		excute_func("Dou_gotAccessToken",[$result]);
	}
	