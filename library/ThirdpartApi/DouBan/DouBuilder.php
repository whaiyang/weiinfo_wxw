<?php
	//Dou_Book 类提供从豆瓣获取的书籍信息的操作
	class Dou_Book
	{
		//$obj为获取的包涵书籍信息的json数据
		public static function getISBN10($obj)
		{
			return $obj->isbn10;
		}
		
		public static function getISBN13($obj)
		{
			return $obj->isbn13;
		}
		
		public static function getTitle($obj)
		{
			return $obj->title;
		}
		
		public static function getImages($obj)//返回数组small medium large
		{
			return array('samll'=>$obj->images->small,'medium'=>$obj->images->medium,'large'=>$obj->images->large);
		}
		
		public static function getAuthor($obj)//返回作者数组
		{
			return $obj->author;
		}
		
		public static function getTranslator($obj)
		{
			return $obj->translator;
		}
		
		public static function getPublisher($obj)//获取出版社
		{
			return $obj->publisher;
		}
		
		public static function getPubdate($obj)//获取出版日期
		{
			return $obj->pubdate;
		}
		
		public static function getTags($obj)//返回标签数组
		{
			$tags = array();
			foreach($obj->tags as $value)
			{
				$tags[] = $value->name;
			}
			return $tags;
		}
		
		public static function getAuthorIntro($obj)
		{
			return $obj->author_intro;
		}
		
		public static function getSummary($obj)
		{
			return $obj->summary;
		}
		
		public static function getAvgScore($obj)
		{
			return $obj->rating->average;
		}
		
		public static function getSqlArr($obj)
		{
			return array('title'=>Dou_Book::getTitle($obj),
				'author'=>Dou_Book::getAuthor($obj),
				'isbn10'=>Dou_Book::getISBN10($obj),
				'isbn13'=>Dou_Book::getISBN13($obj),
				'images'=>implode('|',Dou_Book::getImages($obj)),
				'translator'=>Dou_Book::getTranslator($obj),
				'publisher'=>Dou_Book::getPublisher($obj),
				'pubdate'=>Dou_Book::getPubdate($obj),
				'author_intro'=>Dou_Book::getAuthorIntro($obj),
				'summary'=>Dou_Book::getSummary($obj),
				'avg_score'=>Dou_Book::getAvgScore($obj),
				'tags'=>Dou_Book::getTags($obj)
				);
		}
	}
	
	class Dou_Movie
	{
		public static function getTitle($obj)
		{
			return $obj->title;
		}
		
		public static function getType($obj)
		{
			return $obj->attrs->movie_type;
		}
		
		public static function getAuthor($obj)//数组
		{
			return $obj->attrs->writer;
		}
		
		public static function getDirector($obj)//数组
		{
			return $obj->attrs->director;
		}
		
		public static function getCountry($obj)
		{
			return $obj->attrs->country;
		}
		
		public static function getDuration($obj)//时长
		{
			return $obj->attrs->movie_duration;
		}
		
		public static function getSummary($obj)
		{
			return $obj->summary;
		}
		
		public static function getImages($obj)//返回数组small medium large
		{
			return array('samll'=>$obj->images->small,'medium'=>$obj->images->medium,'large'=>$obj->images->large);
		}
		
		public static function getLang($obj)//获取影片语言
		{
			return $obj->attrs->language;
		}
		
		public static function getPubdate($obj)//获取放映日期 
		{
			return $obj->attrs->pubdate;
		}
		
		public static function getYear($obj)
		{
			return $obj->year;
		}
		
		public static function getAvgScore($obj)
		{
			return $obj->rating->average;
		}
		
		public static function getTags($obj)//获取标签数组
		{
			return $obj->genres;
		}
		
		public static function getSqlArr($obj)
		{
			return array('title'=>Dou_Movie::getTitle($obj),
				'images'=>implode('|',Dou_Book::getImages($obj)),
				'pubdate'=>Dou_Movie::getPubdate($obj),
				'summary'=>Dou_Movie::getSummary($obj),
				'languages'=>Dou_Movie::getLang($obj),
				'avg_score'=>Dou_Movie::getAvgScore($obj),
				'tags'=>Dou_Movie::getTags($obj)
				);
		}
	}