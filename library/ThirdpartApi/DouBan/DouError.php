<?php
	class DouError
	{
		public static $access_token_has_expired = 106;//access_token过期
		public static $invalid_credencial1  = 108;// 用户未授权访问此数据
		public static $username_password_mismatch = 120;//用户名密码不匹配
		public static $invalid_user = 121;//用户不存在或已删除
		public static $user_has_blocked = 122;//用户已被屏蔽
	}