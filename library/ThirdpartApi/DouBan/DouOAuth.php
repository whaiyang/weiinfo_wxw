<?php
	
	//豆瓣API入口
	function Dou_getAuthCode($scopes)
	{
		$dou_uri = "https://www.douban.com/service/auth2/auth";
		$client_id = Dou_APIKey;
		$response_type = "code";
		$scope = "";
		for($i=0;$i<count($scopes);$i++)
		{
			$scope .= $scopes[$i];
			if($i<count($scopes)-1)
				$scope .= ",";
		}
		$request_uri = $dou_uri.'?'.'client_id='.$client_id.'&redirect_uri='.Dou_redirect_uri.
					   '&response_type='.$response_type.'&scope='.$scope;
		header("Location:".$request_uri);
	}
	
	//获取授权用户信息
	function Dou_getDouUser()
	{
		$obj = null;
		$access_token = false;
		if(($access_token=GET_SESSION("access_token"))!=false)
		{
			$uri = "https://api.douban.com/v2/user/~me";
			$result = do_post_request($uri,"","GET",array("Authorization"=>$access_token));
			$obj = json_decode($result);
		}
		return $obj;
	}