<?php
	function str_mysql_addslashes($str){
		if(!get_magic_quotes_gpc()){
			return addslashes($str);
		}
		return $str;
	}

	function str_mysql_stripslashes($str,$sc_treat=false){
		if($sc_treat)
			return htmlspecialchars(stripslashes($str));
		else
			return stripslashes($str);
	}

	function substrs($content,$length='30') { 
		if($length && strlen($content)>$length) { 
			$num=0; 
			for($i=0;$i<$length-3;$i++) { 
				if(ord($content[$i])>127) { 
					$num++; 
				} 
			} 
			$num%2==1 ? $content=substr($content,0,$length-4):$content=substr($content,0,$length-3); 
		} 
		return $content; 
	}
?>