<?php
	class DB{
		private $host;
		private $user_name;
		private $user_password;
		private $db_name;

		public $db = null;

		function __construct($host,$user_name,$user_password,$db_name){
			$this->db_name = $db_name;
			$this->user_name = $user_name;
			$this->user_password = $user_password;
			$this->host = $host;
		}

		public function open(){
			@$this->db = new mysqli($this->host,$this->user_name,$this->user_password,$this->db_name);
			if(mysqli_connect_errno()){
				return false;
			}
			$this->db->query("set names 'utf8'");//very important!
			return true;
		}

		public function close(){
			$this->db->close();
		}
		
		function insert_id()
		{
			return $this->db->insert_id;
		}

		function query($sql,&$affected_rows=null){
			//$this->open();
			$result = $this->db->query($sql);
			@$affected_rows = $this->db->affected_rows;
			//$this->close();
			return $result;
		}

		function select($arr_select,$arr_where,$table,$mode='arr',$extra=''){
			$sql = 'SELECT ';
			$sql2 = 'WHERE ';
			$result;
			$num_results;
			$temp_arr = null;
			$arr_select_size = count($arr_select);
			$arr_where_size = count($arr_where);
			$n = 1;

			foreach($arr_select as $value){
				$sql.=($value.' ');
				if($n++!=$arr_select_size)
					$sql.=',';
			}
			$n=1;
			foreach($arr_where as $key=>$value){
				if(!$this->exit_such_opes($key))
					$sql2.=("`".$key."`='".$value."' ");
				else
					$sql2.=("`".$key."`".$value." ");
				if($n++!=$arr_where_size)
					$sql2.='AND ';
			}
			
			if(!empty($arr_where))
				$sql.=('FROM `'.$table.'` '.$sql2);
			else
				$sql.=('FROM `'.$table."`");

			if(!empty($extra))
				$sql.=" ".$extra;
			
			$result = $this->query($sql);//echo $sql;
			if(!$result)
				return null;
			$num_results = $result->num_rows;
			switch($mode){
				case 'arr':
					for($i=0;$i<$num_results;$i++){
						$temp_arr[$i] = $result->fetch_assoc(); 	
					}
					$result->free();
					return $temp_arr;
					break;
				case 'obj':
					for($i=0;$i<$num_results;$i++){
						$temp_arr[$i] = $result->fetch_object(); 	
					}
					$result->free();
					return $temp_arr;
					break;
				default:
					return null;
			}
		}

		function exit_such_opes($str,$opes=array('>','<','>=','<=','!=','<>','LIKE')){
			foreach($opes as $ope){
				if(strpos($str,$ope)>-1)
					return true;
			}
			return false;
		}

		function insert($arr,$table){
			$arr_size = count($arr);
			$n = 1;
			$sql = 'INSERT INTO '.$table.'( ';
			$sql2 = ' VALUES(';

			foreach($arr as $key=>$value){
				$sql.='`'.$key.'`';
				$sql2.=("'".$value."'");
				if($n++!=$arr_size){
					$sql.=',';
					$sql2.=',';
				}
			}

			$sql.=(')'.$sql2.')');
			return $this->query($sql);
		}

		function update($arr_new,$arr_old,$table,&$affected_rows=null){
			$sql='UPDATE `'.$table.'` SET ';
			$sql2=' WHERE ';
			$n = 1;

			$arr_old_count = count($arr_old);
			$arr_new_count = count($arr_new);
			foreach($arr_new as $key=>$value){
				$sql.=("`".$key."` = '".$value."'");
				if($n++!=$arr_new_count)
					$sql.=' , ';
			}
			$n=1;
			foreach($arr_old as $key=>$value){
				$sql2.=("`".$key."` = '".$value."'");
				if($n++!=$arr_old_count)
					$sql2.=' AND ';
			}
			$sql.=$sql2;
			return $this->query($sql,$affected_rows);
		}

		function delete($arr,$table){
			$sql = 'DELETE FROM '.$table.' WHERE ';
			$arr_size = count($arr);
			$n = 1;
			foreach($arr as $key=>$value){
				$sql.=("`".$key."` = '".$value."'");
				if($n++!=$arr_size)
					$sql.=' AND ';
			}//echo $sql;
			$n===1?null:$this->query($sql,$n);//防止删除整个表
			return $n;
		}

		//ADD ON
		function get_all($SQL)
		{
			$result = $this->query($SQL);//echo $sql;
			if(!$result)
				return null;
			$re = array();
			for($i=0;$i<$result->num_rows;$i++)
			{
						$re[] = $result->fetch_assoc(); 	
			}
			$result->free();
			return $re;
		}
	}