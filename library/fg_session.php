<?php
	function GET_SESSION($id){
		@session_start();
		if(empty($_SESSION[$id]))
			return false;
		else
			return $_SESSION[$id];
	}

	function SET_SESSION($id,$value){
		@session_start(); 
		$_SESSION[$id] = $value;
	}

	function UNSET_SESSION($id){
		@session_start();
		unset($_SESSION[$id]);
	}
	
	//time 过期时间
	function SQL_SET_SESSION($id,$value,$time,$db)
	{
		$perm = $time==0?1:0;
		$t = SQL_GET_SESSION($id,$db);
		if(empty($t))
			return $db->insert(array("key"=>$id,"value"=>$value,"time"=>$time,"permanent"=>$perm),SESSION);
		else
			return $db->update(array("value"=>$value),array("key"=>$id),SESSION);
	}
	
	//更新session时间
	function SQL_UPDATE_SESSION($id,$time,$db)
	{
		return $db->update(array("time"=>$time),array("key"=>$id),SESSION);
	}
	
	function SQL_GET_SESSION($id,$db)
	{
		$result = $db->select(array("*"),array("key"=>$id),SESSION);
		return $result[0]["value"];
	}
	
	function SQL_UNSET_SESSION($id,$db)
	{
		return $db->delete(array("key"=>$id),SESSION);
	}
	
	//删除过期session
	function SQL_UNSET_OT_SESSION($curtime,$db)
	{
		return $db->db->query("delete from ".SESSION." where time<=".$curtime." and permanent<>1");
	}
?>