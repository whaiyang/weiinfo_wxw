<?php
	class Controller
	{
		public $tpl = null;
		public $className = "";
		public $actionName = "";
		public $defaultFunc = "index";
		public $defaultViewDir = "";

		public function __construct($tpl,$className,$actionName)
		{
			$this->tpl = $tpl;
			$this->className = $className;
			$this->actionName = $actionName;
		}

		public function __initialize()
		{

		}

		public function assign($key_or_arr,$value=null)
		{
			$this->tpl->assign($key_or_arr,$value);
		}

		public function display($path="")
		{
			if(!empty($path))
				$this->tpl->display($this->defaultViewDir.$path);
			else
				$this->tpl->display($this->defaultViewDir.$this->actionName.".html");
		}
	}