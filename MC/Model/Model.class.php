<?php
	class Model
	{
		public $db = null;

		public function __construct($db)
		{
			$this->db = $db;
		}

		public function query($sql)
		{
			return $this->db->query($sql);
		}

		public function get_all($SQL)
		{
			return $this->db->get_all($SQL);
		}
	}