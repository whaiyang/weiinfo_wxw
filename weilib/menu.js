/**
*  description: 封装menu json对象的产生
*  @author: Bill Xue
*  @date: 2014/4/17
*/

var menu = {
	createNew : function (){
		var menu = {};
		menu.formated = {
			"button" : []
		}; // formated menu in json 

		maxtopMenulen = 3; // max top menu number that API allowed
		maxsubMenulen = 5; // max sub menu number
		menu.toplen = function(){
			// get the number of top menu
			return menu.formated["button"].length;
		}
		menu.maxsublen = function(id){
			// get max length of submenus
			var  maxlen = 0;
				// this top-menu contains sub-menu
			if (typeof(menu.formated["button"][id]["sub_button"]) != "undefined")
			{
				var len = menu.formated["button"][id]["sub_button"].length;
				maxlen = maxlen > len ? maxlen : len; 
			}
			return maxlen;
		}
		
		// if success, return true, else return false
		// 3 mode:
		// addtopmenu(id, name): only add name, wait for add action or sub-menu
		// addtopmenu(id, null, "click", key): add name and click-action
		// addtopmenu(id, null, "view", null, url): add name and view-action
		menu.addtopmenu = function(id, name, type, key, url){
			var newMenu;
			if (menu.toplen() >= maxtopMenulen){
				return false;
			}

			if ((id < 0) || (id >= maxtopMenulen)){
				return false;
			}
			else{
				if (name){
					newMenu = {"name": name};
					menu.formated.button.push(newMenu);
				}
				else{
					//console.log("in");
					if (type == "view"){
						menu.formated.button[id]["type"] = type;
						menu.formated.button[id]["url"] = url;
					}
					else if(type == "click"){
						menu.formated.button[id]["type"] = type;
						menu.formated.button[id]["key"] = key;
					}
					else{
						return false;
					}
				}

				return true;
			}
		}

		// if success, return true, else return false
		// 3 mode:
		// addsubmenu(pid, cname): only add sub-menu name
		// addsubmenu(pid, null, cid, "view", null, url): add top-menu[pid] sub-menu[cid] properties
		// addsubmenu(pid, null, cid, "click", key): add top-menu[pid] sub-menu[cid] properties
		menu.addsubmenu = function(pid, cname, cid, type, key, url){
			var newSubMenu;
			if ((menu.maxsublen(pid) > maxsubMenulen) || (pid >= menu.formated.button.length) || (pid < 0)){
				return false;
			}
			if (typeof(menu.formated.button[pid].sub_button) == "undefined"){
				// add "sub_button":[] 
				menu.formated.button[pid]["sub_button"] = [];
			}

			if (cname){
				newSubMenu = {"name": cname};
				menu.formated.button[pid].sub_button.push(newSubMenu);
			}
			else{
				if ((cid < 0) || (cid >= menu.formated.button[pid].sub_button.length)){
					return false;
				}

				if (type == "view"){
					menu.formated.button[pid].sub_button[cid]["type"] = type;
					menu.formated.button[pid].sub_button[cid]["url"] = url;
				}
				else if(type == "click"){
					menu.formated.button[pid].sub_button[cid]["type"] = type;
					menu.formated.button[pid].sub_button[cid]["key"] = key;
				}
				else{
					return false;
				}
			}
			return true;
		}

		// if success, return true, else return false
		menu.droptopmenu = function(id){
			if ((id >= menu.formated.button.length) || (id < 0)){
				return false;
			}
			else{
				menu.formated.button.splice(id, 1);
				return true;
			}
		}

		// if success, return true, else return false
		// 2 mode:
		// dropsubmenu(pid): drop all sub-menu of top-menu[pid]
		// dropsubmenu(pid, cid): drop sub-menu[cid] of top-menu[pid]
		menu.dropsubmenu = function(pid, cid){
			if ((pid >= menu.formated.button.length) || (pid < 0)){
				return false;
			}
			if ((cid >= menu.formated.button[pid].sub_button.length) || (cid < 0)){
				return false;
			}
			menu.formated.button[pid].sub_button.splice(cid, 1);
			if (menu.formated.button[pid].sub_button.length == 0){
				delete menu.formated.button[pid].sub_button;
			}
			return true;
		}
		menu.edittopmenu = function(id, name){
			menu.formated.button[id].name = name;
		}
		menu.editsubmenu = function(pid, cid, cname){
			menu.formated.button[pid].sub_button[cid].name = cname;
		}
		return menu;
	}
}