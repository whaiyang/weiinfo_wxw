<?php
	require("userOperation.php");
	
	$MODE = "";
	function Status($status,$arr)
	{
		$json = '{"status":'.$status.',';
		foreach($arr as $key=>$value)
		{
			$json .= '"'.$key.'":'.$value.',';
		}
		$json .='}';
		return json_decode($json);
	}
	
	function setMode($id,$mode,$db)
	{
		return SQL_SET_SESSION($id,$mode,time()+SESS_KEEP_TIME,$db);
	}
	
	function updateMode($id,$db)
	{
		return SQL_UPDATE_SESSION($id,time()+SESS_KEEP_TIME,$db);
	}
	
	function getMode($id,$db)
	{
		return SQL_GET_SESSION($id,$db);
	}
	
	function unsetMode($id,$db)
	{
		return SQL_UNSET_SESSION($id,$db);
	}
	
	function clearMode($db)
	{
		return SQL_UNSET_OT_SESSION(time(),$db);
	}
	//模块进入提示
	function bookMode($msg)
	{
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'您已进入书籍板块','description'=>'输入书名搜书吧
		'.
		'输入 退出 即可退出该板块  输入 板块 即可查询自己所在板块','picUrl'=>getPicUrl("book.jpg"),'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	function movieMode($msg)
	{
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'您已进入影片板块','description'=>'输入片名搜片吧
		'.
		'输入 退出 即可退出该板块  输入 板块 即可查询自己所在板块','picUrl'=>getPicUrl("movie.jpg"),'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	function videoMode($msg)
	{
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'您已进入视频板块','description'=>'请求格式:关键字,发布时间,类别,时间>=,时间<=    输入 退出 即可退出该板块  输入 板块 即可查询自己所在板块','picUrl'=>getPicUrl("youku.jpg"),'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	function weiboMode($msg)
	{
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'输入 退出 即可退出该板块','description'=>"",'picUrl'=>getPicUrl("weibo.jpg"),'url'=>''));
		$msg->addItem(array('title'=>'热门微博','description'=>'','picUrl'=>'','url'=>'http://m.baidu.com/ssid=0/from=2001c/bd_page_type=1/uid=899A3CFB7992309871324F23AFEDFFD1/pu=sz%401320_1003%2Cta%40iphone_2_4.1_1_9.0%2Cusm%400/baiduid=B09BF3EE6A871B719BFB07816E555BBF/w=0_10_%E7%83%AD%E9%97%A8%E5%BE%AE%E5%8D%9A/t=iphone/l=3/tc?ref=www_iphone&lid=15342062786866583220&order=1&vit=osres&tj=www_normal_1_0_10&m=8&srd=1&cltj=cloud_title&dict=21&sec=32335&di=6e45592396d94feb&bdenc=1&nsrc=ZYnVPgEptyoA_yixCFOxXnANedT62v3IHhmJ_zdV_zi695qshbWxBa'));
		$msg->addItem(array('title'=>'发微博','description'=>'','picUrl'=>'','url'=>'http://widget.weibo.com/dialog/PublishWeb.php?refer=y&language=zh_cn&app_src=37hi36&button=pubilish'));
		$msg->changeTarget();
		return $msg;
	}
	
	function tourMode($msg)
	{
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'您已进入旅游板块','description'=>'输入 退出 即可退出该板块  输入 板块 即可查询自己所在板块','picUrl'=>getPicUrl("tour.jpg"),'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	//根据用户所在板块路由至相应函数
	function routing($msg,$content,$id,$mode,$db=null)
	{
		switch($content)
		{
			case "退出":
			case "板块":
				return null;
				break;
			case "帮助":
			case "help":
				return onSubscribe($msg,$db,true);
				break;
		}
		
		if(isset($mode))
		{
			switch($mode)
			{
				case "书籍":
					return books($msg,$content);
					break;
				case "影片":
					return movies($msg,$content);
					break;
				case "视频"://关键字 ，发布时间，类别，时间>=，时间<=
					$content = str_replace("，",",",$content);
					$arr = explode(",",$content);
					$keyword = $arr[0];
					$period = empty($arr[1])?"":$arr[1];//视频上传时间
					$category = empty($arr[2])?"":$arr[2];//类别
					$timemore = empty($arr[3])?"":$arr[3];//>=
					$timeless = empty($arr[4])?"":$arr[4];//<=
 					return videos($msg,$keyword,$period,$category,0,5,$timeless,$timemore);
					break;
			}
		}
		else
		{
			return null;
		}
	}
	
	function checkMode($msg,$id,$db)
	{
		$mode = getMode($id,$db);
		if(empty($mode))
		{
			$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			$msg->addItem(array('title'=>'您不在任何板块','description'=>'输入 书籍 影片 视频 微博 旅游 即可进入相应板块','picUrl'=>"",'url'=>''));
			$msg->changeTarget();
			return $msg;
		}
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>$mode.'板块','description'=>'','picUrl'=>"",'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	function quitModeMsg($msg,$id,$db)
	{
		$mode = getMode($id,$db);
		if(empty($mode))
		{
			return checkMode($msg,$id,$db);
		}
		unsetMode($msg->getFUserN(),$db);
		$msg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$msg->addItem(array('title'=>'您已退出'.$mode.'板块','description'=>'输入 书籍 影片 视频 微博 旅游 即可进入相应板块','picUrl'=>"",'url'=>''));
		$msg->changeTarget();
		return $msg;
	}
	
	//优先处理
	function preOpe($msg,$content,$db)
	{
		$result = news($msg,$content);
		if(isset($result))
			return $result;
		$result = getMusic($msg,$content);
		if(isset($result))
			return $result;
	}
	
	//板块选择及预处理
	function wordsPreOpe($msg,$content,$db)
	{
		$result = preOpe($msg,$content,$db);//优先处理
		$content = strtolower($content);
		if(isset($result))
			return $result;
		switch($content)
		{		
			case "注册":
				return user($msg,$db);
				break;
			case "新闻":
				return news($msg);
				break;
			case "影片":
			case "电影":
				setMode($msg->getFUserN(),"影片",$db);
				return movieMode($msg);
				break;
			case "书籍":
			case "书":
			case "book":
				setMode($msg->getFUserN(),"书籍",$db);
				return bookMode($msg);
				break;
			case "视频":
				setMode($msg->getFUserN(),"视频",$db);
				return videoMode($msg);
				break;
			case "微博":
				setMode($msg->getFUserN(),"微博",$db);
				return weiboMode($msg);
				break;
			case "旅游":
				setMode($msg->getFUserN(),"旅游",$db);
				return tourMode($msg);
				break;
			case "板块":
				return checkMode($msg,$msg->getFUserN(),$db);
				break;
			case "退出":
				return quitModeMsg($msg,$msg->getFUserN(),$db);
				break;
		}
		return null;
	}
	
	function QA($msg,$content,$db)
	{
				$content = str_mysql_addslashes($content);
				@$is_insert = preg_match("/QA:|：[0-9]+:|：(.*?)/is",$content);
				if(!$is_insert)
				{
					$query = $db->db->query('select * from `q_and_a` where `q` like "%'.$content.'%"');//$db->select(array('*'),array("q LIKE \"%"=>$content."%\""),"q_and_a");
					$result = $query->fetch_assoc();
					$query->free();
					if(isset($result)&&!intval($result["is_null"]))
					{
						$msg->response(str_mysql_stripslashes($result["a"]));
						return $msg;
					}
					else
					{
						$id;
						if(isset($result))
							$id = $result["id"];
						else
						{
							$result2 = $db->insert(array("q"=>$content,"a"=>" "),"q_and_a");
							if(!$result2)
							{
								$msg->response("我还不知道这个问题的答案哦");
								return $msg;
							}
							$result2 = $db->select(array("*"),array("q"=>$content),"q_and_a");
							$result2 = $result2[0];
							$id = $result2["id"];
						}
						$msg->response("我还不知道这个问题的答案哦!\n回复 QA:".$id.":答案 就能教会我哦！");
						return $msg;
					}
				}
				else
				{
					$content = $msg->getContent();
					$content = str_replace("：",":",$content);
					$reply_client = explode(":",$content,3);
					$id = $reply_client[1];
					$a = str_mysql_addslashes($reply_client[2]);
					$query = $db->select(array('*'),array('id'=>$id),"q_and_a");
					$query = $query[0]["is_null"];
					if($query==0)
					{
						$msg->response("我已经知道这个问题的答案了哦");
						return $msg;
					}
					@$result = $db->update(array("a"=>$a,"is_null"=>0),array("id"=>$id),"q_and_a");
					if($result)
					{
						$msg->response("我知道这个问题的答案了哦");
					}
					else
					{
						$msg->response("额。。。能再教我一次吗");
					}
					return $msg;
				}
	}
	
	//词汇过滤
	function wordsFilter($msg)
	{
		$content = $msg->getContent();
		$patterns = getWords();
		$content = preg_replace($patterns,"\\1",trim($content));//过滤词汇
		return $content;
	}
		
	function operation($msg,$db)
	{
		clearMode($db);//清除过期session
		$weiid = $msg->getFUserN();//用户微信号
		updateMode($weiid,$db);//延长session
		$MODE = getMode($weiid,$db);//所在板块
		$status = null;
		switch($msg->getMSGType())
		{
			case "text":
				$content = wordsFilter($msg);//词汇过滤
				$status = routing($msg,$content,$weiid,$MODE,$db);//功能路由
				if($status != null)
					return $status;
				$status = wordsPreOpe($msg,$content,$db);//特殊词汇预处理
				if($status != null)
					return $status;
				$msg = QA($msg,$content,$db);//智能问答
				return $msg;
				break;
			case "event":
				if($msg->getEvent()=="subscribe")		
					return onSubscribe($msg,$db);
				else if($msg->getEvent()=="CLICK")
					return onClick($msg,$db);
				break;
		}
	}
	