<?php
	require_once("music/MusicSearch.php");	
	
	function onSubscribe($msg,$db,$forceSend=false)
	{
		$event = "";
		if($msg->getMSGType()=="event")
			$event = $msg->getEvent();
		if($event=="subscribe"||$forceSend)
		{
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			$sendMsg->addItem(array('title'=>'微信息--提供您最感兴趣的信息','description'=>'','picUrl'=>getPicUrl("cover.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'大片和影评 (输入 影片)','description'=>'','picUrl'=>getPicUrl("movie.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'旅游 (输入 旅游)','description'=>'','picUrl'=>getPicUrl("tour.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'热门微博 (输入 微博)','description'=>'','picUrl'=>getPicUrl("weibo.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'海量视频 (输入 视频)','description'=>'','picUrl'=>getPicUrl("youku.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'书籍搜索 (输入 书籍)','description'=>'','picUrl'=>getPicUrl("book.jpg"),'url'=>''));
			$sendMsg->addItem(array('title'=>'时事资讯 (输入 新闻)
输入以下语句即可查看更多新闻：

国际(新闻),国内(新闻),军事(新闻),
社会(新闻),游戏(新闻),财经(新闻),
房产(新闻),汽车(新闻),体育(新闻),
娱乐(新闻),教育(新闻),科技(新闻),
女人(女性),互联网(新闻)','description'=>'','picUrl'=>getPicUrl("news.jpg"),'url'=>''));
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}

	/**
		微信菜单点击事件
	*/
	function onClick($msg,$db)
	{
		$sendMsg = null;
		switch ($msg->getEventKey()) {
			case '0_1':
				$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
				$sendMsg->addItem(array('title'=>'幸运大转盘','description'=>'','picUrl'=>getPicUrl("roulette_cover.jpg"),'url'=>setController("roulette","index",$msg->getFUserN())));
				break;
		}
		if(!empty($sendMsg))
		{
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}
	
	function user($msg,$db)
	{
		$result = Sql::WeiUser_createUser($msg->getFUserN(),$db);
		if($result->status===false)
		{
			$msg->response("您已经注册了");
			return $msg;
		}
		else
		{
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			$sendMsg->addItem(array("title"=>"您的用户名:".$result->name,'description'=>'点击完善个人信息以获得更多服务','picUrl'=>getPicUrl("cover.jpg"),'url'=>setUrl(array('weiid'=>$msg->getFUserN(),'target'=>'user_info_consume'))));
			$sendMsg->changeTarget();
			return $sendMsg;
		}
	}
	
	function getMusic($msg,$content)
	{
		$arr = explode(" ",$content);
		if(strpos($arr[0],"音乐")>-1&&!empty($arr[1]))
		{
			$arr[1] = str_replace("，",",",$arr[1]);
			$arrK = explode(",",$arr[1]);
			$keyword = $arrK[0];
			$author = empty($arrK[1])?"":$arrK[1];
			$musics = getMusics($keyword,$author);
			if(count($musics)==0)
			{
				$msg->response("没有找到匹配的音乐");
				return $msg;
			}
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_MUSIC );
			$sendMsg->addMusic(array('title'=>$keyword,'description'=>"尽情享受你的音乐",'musicUrl'=>$musics[0]->src,'HQMusicUrl'=>$musics[0]->src));
			return $sendMsg;
		}
		else
			return null;
	}
	
	function videos($msg,$keyword,$time="",$category="",$offset=0,$count=5,$timeless=0,$timemore=0)
	{
		$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$videos = Youku_searchVideo($keyword,$time,$category,$offset,$count,$timeless,$timemore);
		$v = new YoukuVideo($videos);
		$title = $v->getTitles();
		$id = $v->getIds();
		$duration = $v->getDurations(true);
		//$flash = $v->getFlashes();
		$image = $v->getImages();
		$num = $v->getCount();
		for($i=0;$i<$num;$i++)
		{
			$sendMsg->addItem(array("title"=>$title[$i]."(".$duration[$i]."分钟)",'description'=>"",'picUrl'=>$image[$i],'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"apikey"=>Youku_APIKEY,"videoid"=>$id[$i],'SIGNATURE'=>md5($id[$i].'_'.time().'_'.Youku_APIKEY),'target'=>'video_play'))));
		}
		if($num<$v->getTotalNum())
		{
			$sendMsg->addItem(array("title"=>"点击查看更多",'description'=>"",'picUrl'=>"",'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"time"=>$time,"category"=>$category,"index"=>0,"count"=>$count,'keyword'=>$keyword,'target'=>'video_list'))));
		}
		$sendMsg->changeTarget();
		return $sendMsg;
	}
	
	function movies($msg,$keyword,$tag="",$offset=0,$count=5)
	{
		$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$obj = Dou_searchMovies_list($keyword,"",0,$count);
		foreach($obj->subjects as $movie)
		{
			$picUrl = Dou_Movie::getImages($movie);
			$picUrl = $picUrl['large'];
			$sendMsg->addItem(array("title"=>Dou_Movie::getTitle($movie)."(".Dou_Movie::getYear($movie).")",'description'=>"",'picUrl'=>$picUrl,'url'=>setUrl(array('weiid'=>$msg->getFUserN(),'keyword'=>$keyword,"movieid"=>$movie->id,"picUrl"=>urlencode($picUrl),'target'=>'movie_detail'))));
		}
		if($count<$obj->total)
		{
			$sendMsg->addItem(array("title"=>"点击查看更多",'description'=>"",'picUrl'=>"",'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"index"=>0,"count"=>$count,'keyword'=>$keyword,'target'=>'movie_list'))));
		}
		$sendMsg->changeTarget();
		return $sendMsg;
	}
	
	function books($msg,$keyword,$tag="",$offset=0,$count=5)
	{
		$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$obj = Dou_searchBooks($keyword,"");//file_get_contents(setUrl(array("keyword"=>$keyword),"API_DOU","DOU_BOOK_SEARCH_KEYWORD"));
		foreach($obj->books as $book)
		{
			$picUrl = Dou_Book::getImages($book);
			$picUrl = $picUrl['large'];
			$sendMsg->addItem(array("title"=>Dou_Book::getTitle($book)."——\n        ".mb_substr(Dou_Book::getSummary($book),0,30,"utf-8")."...",'description'=>"",'picUrl'=>$picUrl,'url'=>setUrl(array('weiid'=>$msg->getFUserN(),'keyword'=>$keyword,"bookid"=>$book->id,'target'=>'book_detail'))));
		}
		if($count<$obj->total)
		{
			$sendMsg->addItem(array("title"=>"点击查看更多",'description'=>"",'picUrl'=>"",'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"index"=>0,"count"=>$count,'keyword'=>$keyword,'target'=>'book_list'))));
		}
		$sendMsg->changeTarget();
		return $sendMsg;
	}
	
	function news($msg,$type="国际新闻")
	{
		$urls = array("5fab1"=>"http://news.baidu.com/n?cmd=1&class=internews&tn=rss",//国际新闻
		"adb02"=>"http://news.baidu.com/n?cmd=1&class=civilnews&tn=rss",//国内新闻
		"ec3ef"=>"http://news.baidu.com/n?cmd=1&class=mil&tn=rss",//军事新闻
		"2864d"=>"http://news.baidu.com/n?cmd=1&class=socianews&tn=rss",//社会新闻
		"f2d90"=>"http://news.baidu.com/n?cmd=1&class=internet&tn=rss",//财经新闻
		"c77ac"=>"http://news.baidu.com/n?cmd=1&class=internet&tn=rss",//互联网新闻
		"2ec69"=>"http://news.baidu.com/n?cmd=1&class=housenews&tn=rss",//房产新闻
		"c9b00"=>"http://news.baidu.com/n?cmd=1&class=autonews&tn=rss",//汽车新闻
		"aec4d"=>"http://news.baidu.com/n?cmd=1&class=sportnews&tn=rss",//体育新闻
		"47814"=>"http://news.baidu.com/n?cmd=1&class=enternews&tn=rss",//娱乐新闻
		"e7372"=>"http://news.baidu.com/n?cmd=1&class=gamenews&tn=rss",//游戏新闻
		"c44f7"=>"http://news.baidu.com/n?cmd=1&class=edunews&tn=rss",//教育新闻
		"16410"=>"http://news.baidu.com/n?cmd=1&class=healthnews&tn=rss",//女人
		"0d39b"=>"http://news.baidu.com/n?cmd=1&class=technnews&tn=rss",//科技新闻
		"9b3b2"=>"http://news.baidu.com/n?cmd=4&class=internews&tn=rss",//国际
		"227dd"=>"http://news.baidu.com/n?cmd=4&class=civilnews&tn=rss",//国内
		"e801f"=>"http://news.baidu.com/n?cmd=4&class=mil&tn=rss",//军事
		"ce4ba"=>"http://news.baidu.com/n?cmd=4&class=socianews&tn=rss",//社会
		"0f078"=>"http://news.baidu.com/n?cmd=4&class=finannews&tn=rss",//财经
		"f27b5"=>"http://news.baidu.com/n?cmd=4&class=internet&tn=rss",//互联网
		"381ae"=>"http://news.baidu.com/n?cmd=4&class=housenews&tn=rss",//房产
		"b64c4"=>"http://news.baidu.com/n?cmd=4&class=autonews&tn=rss",//汽车
		"b0dc2"=>"http://news.baidu.com/n?cmd=4&class=sportnews&tn=rss",//体育
		"9acf9"=>"http://news.baidu.com/n?cmd=4&class=enternews&tn=rss",//娱乐
		"ba082"=>"http://news.baidu.com/n?cmd=4&class=gamenews&tn=rss",//游戏
		"8640c"=>"http://news.baidu.com/n?cmd=4&class=edunews&tn=rss",//教育
		"188e0"=>"http://news.baidu.com/n?cmd=4&class=healthnews&tn=rss",//女性
		"3d078"=>"http://news.baidu.com/n?cmd=4&class=technnews&tn=rss");//科技
		$type = substr(md5($type),0,5);
		if(isset($urls[$type]))
		{
			$url = $urls[$type];
			$rss = new RssReader();
			$rss->get($url);
			$count = 8;
			$newsArr = $rss->getMaxFObj(0,$count);
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			$i = 0;
			foreach($newsArr as $value)
			{//RssReader::getImage($value)
				$sendMsg->addItem(array("title"=>$value->title,'description'=>"",'picUrl'=>"",'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"index"=>$i,'url'=>urlencode($url),'target'=>'news'))));
				$i++;
			}
			$sendMsg->addItem(array("title"=>"点击查看更多",'description'=>"",'picUrl'=>"",'url'=>setUrl(array('weiid'=>$msg->getFUserN(),"index"=>0,"count"=>8,'url'=>urlencode($url),'target'=>'list'))));
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
			return null;
	}