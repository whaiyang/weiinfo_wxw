<?php
	function onSubscribe($msg,$db,$forceSend=false)
	{
		$event = "";
		if($msg->getMSGType()=="event")
			$event = $msg->getEvent();
		if($event=="subscribe"||$forceSend)
		{
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_TEXT);
			$sendMsg->setContent("输入 注册 我要提问 我要回答 我的信息 来选择功能");
			//$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_VOICE);
			//$sendMsg->setMediaId(123);
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}

	function onInput($msg,$db)
	{
		$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
		$pic = "";
		switch($msg->getContent())
		{
			case "我要提问":
				$pic = getPicUrl("question.jpg");	
				$sendMsg->addItem(array('title'=>"点击提问",'description'=>'','picUrl'=>$pic,'url'=>getTpUrl("Ty/question",null)));
				break;
			case "我要回答":
				$pic = getPicUrl("answer.jpg");	
				$sendMsg->addItem(array('title'=>"点击回答",'description'=>'','picUrl'=>$pic,'url'=>getTpUrl("Ty/list",null)));
				break;
			case "注册":
				$pic = getPicUrl("register.jpg");	
				$sendMsg->addItem(array('title'=>"点击注册",'description'=>'','picUrl'=>$pic,'url'=>getTpUrl("Ty/register",null)));
				break;
			case "我的信息":
				$pic = getPicUrl("register.jpg");	
				$sendMsg->addItem(array('title'=>"我的信息",'description'=>'','picUrl'=>$pic,'url'=>getTpUrl("Ty/profile",null)));
				break;
		}

		if(!empty($sendMsg))
		{
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}

	/**
		微信菜单点击事件
	*/
	function onClick($msg,$db) 
	{
		$sendMsg = null;
		$cateid = $msg->getEventKey();
		$count = $db->query("select count(*) as total from dux_category where parent_id=$cateid");
		$count = $count->fetch_assoc();
		$count = intval($count["total"]);
		if($count>0)
		{
			$cates = $db->get_all("select * from dux_category where parent_id=$cateid order by sequence asc limit 0,10");
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			foreach($cates as $key=>$value)
			{
				$pic = "";
				if(!empty($value['image']))
					$pic = getCmsPic($value['image']);
				$sendMsg->addItem(array('title'=>$value['name'],'description'=>'','picUrl'=>$pic,'url'=>setCmsUrl("xrxy","article/Category/index",array('class_id'=>$value['class_id']))));
			}
		}
		else
		{
			$count = $db->query("select count(*) as total from dux_content where class_id=$cateid");
			$count = $count->fetch_assoc();
			$count = intval($count["total"]);
			$article_num = 10;
			$more = false;
			if($count>10)
			{
				$article_num = 9;
				$more = true;
			}
			$article = $db->get_all("select * from dux_content where class_id=$cateid order by content_id desc limit 0,$article_num");
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			foreach($article as $key=>$value)
			{
				$pic = "";
				if(!empty($value['image']))
					$pic = getCmsPic($value['image']);
				$sendMsg->addItem(array('title'=>$value['title'],'description'=>'','picUrl'=>$pic,'url'=>setCmsUrl("xrxy","article/Info/index",array('content_id'=>$value['content_id']))));
			}
			if($more)
			{
				$sendMsg->addItem(array('title'=>"查看更多",'description'=>'','picUrl'=>'','url'=>setCmsUrl("xrxy","article/Category/index",array('class_id'=>$cateid))));
			}
		}
		if(!empty($sendMsg))
		{
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}