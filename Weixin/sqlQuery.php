<?php
	require("userOperation.php");
	
	function operation($msg,$db)
	{
		switch($msg->getMSGType())
		{
			case "text":
				return onInput($msg,$db);
				break;
			case "event":
				if($msg->getEvent()=="subscribe")		
					return onSubscribe($msg,$db);
				else if(strtolower($msg->getEvent())=="click")
					return onClick($msg,$db);
				break;
		}
	}
	