<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<meta charset="utf-8">
<title>WeiQA</title>
<link href="<?=PUBDIR?>/css/style.css" rel="stylesheet" type="text/css"  />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function getArgs() { 
		var args = {};
        var query = location.search.substring(1);
         // Get query string
		var pairs = query.split("&"); 
                    // Break at ampersand
		for(var i = 0; i < pairs.length; i++) {
            var pos = pairs[i].indexOf('=');
             // Look for "name=value"
            if (pos == -1) continue;
                    // If not found, skip
                var argname = pairs[i].substring(0,pos);// Extract the name
                var value = pairs[i].substring(pos+1);// Extract the value
                //value = decodeURIComponent(value);// Decode it, if needed
                args[argname] = value;
                        // Store as a property
        }
		return args;// Return the object 
	}
	
	function link(e)
	{
		event = e||event;
		target = event.target||event.srcElement; 
		location.href = target.getAttribute("link");
	}
	
	function scrollToBottom()
	{
		window.scrollTo(0, document.body.scrollHeight);
	}
</script>
<script type="text/javascript">
	var $_GET = getArgs();
	var weiid = $_GET['weiid'];
	var movieid = $_GET['movieid'];
	var keyword = $_GET['keyword'];
	var picUrl = $_GET['picUrl'];
		
	$(document).ready(function()
	{	
		$("#clickToLoad").hide();
		$("#clickToLoad").click(function()
		{
			$("#loadTxt").hide();
			$("#loadErr").hide();
			$("#loadImg").show();
			$.get("http://"+location.hostname+location.pathname+"?action=weixin&sub_action=WEB&target=loadMovies.php&weiid="+weiid+"&picUrl="+picUrl+"&movieid="+movieid+"&keyword="+keyword,
			function(data,status){
					$("#loadImg").hide();
					if(data.indexOf("NO_MORE")>-1)
					{
						$("#clickToLoad").html("没有更多了");
						$("#clickToLoad").attr({"disabled":"disabled"});
					}
					else
					{
						$("#logs").append(data);
						$("#loadTxt").show();
					}
			}).fail(function(){
				$("#loadImg").hide();
				$("#loadErr").show();
			});
			index += count;
		});
		
		$("#tag_page .tag_box").hide();
		$(".tag_head_button").each(function(){
			$(this).click(function(){
				$("#tag_page .tag_box").hide();
				$("#tag_page .tag_box").eq($(this).index()).show(500,function(){scrollToBottom();});
			});
		});
		
		$("#clickToLoad").click();
	});
</script>
</head>
<body>
    <div class="header">
        <div class="logo">
            <a href="index.html"><h1>WeiQA</h1></a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="content">
		<div id="logs">
		</div>		
        <div class="pagination">
			<button class="clickToLoad" id="clickToLoad"><img id="loadImg" style="height:30px;display:none;" src="images/load.gif"></img><span id="loadTxt">点击加载更多</span><span id="loadErr" style="display:none;">加载失败</span></button>
           <!-- <ul>
                <li><a href="#"><img src="images/right-arrow.png" alt=""></a></li>
                <li><a href="#">1</a></li>
                <li class="current"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#"><img src="images/left-arrow.png" alt=""></a></li>
            </ul>-->
        </div>
    </div>
<!--TAG-->
		<style type="text/css">
			/*
	TAG
*/
#tag_head
{
	border-bottom: 2px solid #eeeeee;
}

.tag_head_button
{
	width:49.4%;
	min-height:35px;
	line-height:35px;
	vertical-align:middle;
	padding:0 2px;
	color:#589c66;
	background-color:#deede0;
	border:1px solid #b8d9be;
	border-bottom:0;
	text-align:center;
	display:inline-block;
}

.clickToLoad_tag,.submit_comment
{
	width:185px;
	min-height:35px;
	line-height:35px;
	vertical-align:middle;
	padding:0 2px;
	color:#ca6445;
	background-color:#fae9da;
	border:1px solid #FFF;
	text-align:center;
	margin:0 25%;
}

.tag_box
{
	display:none;
}

.tag_item
{
	border-bottom:2px solid #eeeeee;
	padding-bottom:20px;
	padding-left:20px;
}

#writeComment
{
	margin-left:5%;
	margin-bottom:10px;
	width:90%;
	height:200px;
}

.weiqa_comment span
{
	font-size:17px;
}

		</style>
		<div id="tag_page">
			<div id="tag_head">
				<button class="tag_head_button" id="tag_head_button1"><span>看评论</span></button>
				<button class="tag_head_button" id="tag_head_button2"><span>说几句</span></button>
			</div>
			<div class="tag_box">
				<div class="content">
					<div id="logs">
					<!--comment-->
						<div class="tag_item">
							<div class="weiqa_comment">
								<p><span class="comm_label">网友：</span><span class="com_uname">why2012</span>&nbsp;&nbsp;<span class="comm_label">发表时间:</span><span class="create_date">2013/7/31</span>&nbsp;&nbsp;<span class="comm_label">最后更新时间:</span><span class="update_date">2013/8/1</span></p>
								<div class="comment_content"><br/>
									&nbsp;&nbsp;很好看的电影
								</div>
							</div>
						</div>
						
						<div class="tag_item">
							<div class="weiqa_comment">
								<p><span class="comm_label">网友：</span><span class="com_uname">why2012</span>&nbsp;&nbsp;<span class="comm_label">发表时间:</span><span class="create_date">2013/7/31</span>&nbsp;&nbsp;<span class="comm_label">最后更新时间:</span><span class="update_date">2013/8/1</span></p>
								<div class="comment_content"><br/>
									&nbsp;&nbsp;很好看的电影
								</div>
							</div>
						</div>
						
					</div>		
					<div class="pagination">
						<button class="clickToLoad_tag" id="clickToLoad_comment"><img id="loadImg" style="height:30px;display:none;" src="images/load.gif"></img><span id="loadTxt">点击加载更多</span><span id="loadErr" style="display:none;">加载失败</span></button>
					</div>
				</div>
			</div>
			<div class="tag_box">
				<div class="content">
					<div id="logs">
						<div id="blog">
							<form action="#" method="post">
								<textarea id="writeComment" name="comment"></textarea>
								<button class="submit_comment"><span id="loadTxt">提交</span></button>
							</form>
						</div>
					</div>		
				</div>
			</div>
		</div>
	<!--TAG-->
<div class="footer">
    <p><a href="#"></a></p>
</div>

</body>
</html>
