﻿<?php
	@$weiid = $_GET['weiid'];
	@$bookid = $_GET['bookid'];
	@$keyword = urldecode($_GET['keyword']);
	@$count = intval($_GET['count']);
	@$index = intval($_GET['index']);
	if(isset($keyword))
	{
		if(isset($bookid))
		{
			$book = Dou_searchBook($bookid);
			$title = Dou_Book::getTitle($book);
			$image = Dou_Book::getImages($book);
			$image = $image["large"];
			$authors = Dou_Book::getAuthor($book);
			$author = "";
			foreach($authors as $str)
				$author .= $str." ";
			@$translators = Dou_Book::getTranslator($book);
			$translator = "";
			foreach($translators as $str)
				$translator .= $str." "; 
			$authorIntro = Dou_Book::getAuthorIntro($book);
			$publisher = Dou_Book::getPublisher($book);
			$pubdate = Dou_Book::getPubdate($book);
			$price = $book->price;
			$desc = Dou_Book::getSummary($book);
			echo "<div class='blog'><h2 style='font-size:22px;'>$title</h2><br/><img src=$image></img><br/><h4 style='font-size:22px;'>作者：</h4>&nbsp;&nbsp;$author<br/><h4 style='font-size:22px;'>翻译：</h4>&nbsp;&nbsp;$translator<br/><h4 style='font-size:22px;'>作者简介：</h4>&nbsp;&nbsp;$authorIntro<br/><h4 style='font-size:22px;'>出版社：</h4>&nbsp;&nbsp;$publisher<br/><h4 style='font-size:22px;'>出版日期：</h4>&nbsp;&nbsp;$pubdate<br/><h4 style='font-size:22px;'>价格：</h4>&nbsp;&nbsp;$price<br/><h4 style='font-size:22px;'>简介：</h4>&nbsp;&nbsp;$desc</div>";
		}
		else
		{
			$books = Dou_searchBooks($keyword,"",$index,$count);
			foreach($books->books as $book)
			{
				if(!empty($book))
				{
					$title = Dou_Book::getTitle($book);
					$image = Dou_Book::getImages($book);
					$image = $image["large"];
					$authors = Dou_Book::getAuthor($book);
					$author = "";
					foreach($authors as $str)
						$author .= $str." ";
					@$translators = Dou_Book::getTranslator($book);
					$translator = "";
					foreach($translators as $str)
						$translator .= $str." "; 
					$authorIntro = Dou_Book::getAuthorIntro($book);
					$publisher = Dou_Book::getPublisher($book);
					$pubdate = Dou_Book::getPubdate($book);
					$price = $book->price;
					$desc = Dou_Book::getSummary($book);
					echo "<div class='blog'><h2 style='font-size:22px;'>$title</h2><br/><img src=$image></img><br/><h4 style='font-size:22px;'>作者：</h4>&nbsp;&nbsp;$author<br/><h4 style='font-size:22px;'>翻译：</h4>&nbsp;&nbsp;$translator<br/><h4 style='font-size:22px;'>作者简介：</h4>&nbsp;&nbsp;$authorIntro<br/><h4 style='font-size:22px;'>出版社：</h4>&nbsp;&nbsp;$publisher<br/><h4 style='font-size:22px;'>出版日期：</h4>&nbsp;&nbsp;$pubdate<br/><h4 style='font-size:22px;'>价格：</h4>&nbsp;&nbsp;$price<br/><h4 style='font-size:22px;'>简介：</h4>&nbsp;&nbsp;$desc</div>";
				}
			}
			if(empty($books->count))
				echo "NO_MORE";
		}
	}   