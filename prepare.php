<?php
	//Config
	require_once("config.php");
	//functions 
	require_once("library/DB.php");
	require_once("library/strmanip.php");
	require_once("library/words.php");
	require_once("library/fg_session.php");
	require_once("library/doPost.php");
	require_once("library/RssReader.php");
	require_once("library/Sql.php");
	require_once("library/DateHelper.php");
	require_once("library/URL.php");
	require_once("library/common.php");
	//ThirdPartAPI
	//require_once("library/ThirdpartApi/DouBan/DouBanAPI.php");
	//require_once("library/ThirdpartApi/Youku/YoukuAPI.php");
	//Time
	DateHelper::setLocation();
	//mySql DB
	$db = new DB(LOC,USERNAME,PASSWORD,DATABASE);
	$db->open();
	//Route
	//Main part
	require_once("route.php");
	//Main
	
	$db->close();
	
	