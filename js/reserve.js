var reserve = {
	// attributions of data
	datebox : Array()
	,

	// attributions of function
	// actions of the pages
	// -reserve page functions
	findDatebox : function() {
		$("input:text").each(function(){
			if ($(this).attr("date") == "1") {
				reserve.datebox.push($(this));
			}
		});
			console.log("find true");
		return false;
	}
	,
	dateboxIni : function() {
		var i;
		for (i in reserve.datebox) {
			$(reserve.datebox[i]).datepicker();
		}
		console.log("init true");
		return false;
	}
	,
	// -libaray page functions
	switchLib : function() {
		$(".lib_switch h5").click(function(){
			var pa = $(this).parent();
			var iBody = pa.find(".lib_body");
			var iHead = pa.find("span");
			var before = $(".lib_switch").not(this).find(".lib_body:visible:first").parent();

			if (pa.find(".lib_body").is(":visible")) {
				iBody.slideUp(150);
				iHead.removeClass("active");
				return false;
			}

			iBody.slideDown(150);
			iHead.addClass("active");
			if (before.length > 0){
				before.find(".lib_body").slideUp(150);
				before.find("span").removeClass("active");
			}
		});
		return false;
	}
	,
	switchSrc : function() {
		$(".lib_body").find("li").click(function(){
			var taget = $(this).attr("data");
			$("#lib_page").find("iframe").attr("src","http://www.w3school.com.cn");
		});
		return false;
	}
	,

	// init functions
	initReserve : function() {
		// find the datepicker boxes
		reserve.findDatebox();
		// datepicker
		reserve.dateboxIni();
	}
	,
	initLib : function() {
		reserve.switchLib();
		reserve.switchSrc();
	}
	// ,
}

$(document).ready(function(){
	reserve.initLib();
});